class SessionsController < ApplicationController
  include SessionsHelper
  def new
  end

  def create
      @user = User.find_by(email: params[:user][:email].downcase)
    if @user && @user.authenticate(params[:user][:password])
      sign_in @user
      render json: { :status => 200, :response => { remember_token: String(@user.id)+"."+@user.remember_token } }
    else
      render json: { :status => 401, :response => { :errorMessages => ["Invalid Email or Password"] } }
    end
  end

=begin def check
    @user = User.find_by(email: params[:user][:email].downcase, remember_token: params[:user][:remember_token])
  if @user
        render json: { :status => 200, :response => { remember_token: @user.remember_token } }
      else
        render json: { :status => 404, :response => { :error => "Invalid remember_token" } }
      end
  end
=end
end
