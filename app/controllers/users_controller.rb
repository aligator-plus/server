class UsersController < ApplicationController
  def show
    @user = User.select('id, username, email').find_by(:id => params[:id])
    if @user
      render json: { :status => 200, :response => @user }
    else
      render json: { :status => 404, :response => { :error => "User is not found" } }
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: { :status => 200, :response => { id: @user.id } }
    else
      render json: { :status => 400, :response => { :errorMessages => @user.errors.full_messages } }
    end
  end

  def index
    render json: {  }
  end

  private

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation) if params.required(:user)
  end
end
