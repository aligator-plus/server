class SitesController < ApplicationController
  def create
    @site = Site.create(rss: site_params[:link])
    cur_site = Site.find_by(host: @site.host)
    if cur_site != nil
      user_id = @user.id
      site_id = cur_site.id
      if(Subscriber.find_by(user_id: user_id,site_id: site_id) == nil)
        Subscriber.create(user_id: user_id,site_id: site_id)
        render json: { :status => 200, :response =>  @site.host }
      else
        render json: { :status => 401, :response => { :errorMessages => ["You are already subscribed to this site"] } }
      end
    else
        render json: { :status => 401, :response => { :errorMessages => ["Invalid site"] } }
    end
  end
  def getusername
    name =(User.select("username").where(id: @user.id))
    username = ""
    username.clear
    name.each{|i| username << i["username"]}
    render json: { :status => 200, :response =>  username }
  end
  def getsite
    render json: (Subscriber.joins("left join sites on sites.id = subscribers.site_id").select("sites.id,sites.host,sites.title").where(user_id: @user.id))
  end
  def getarticle
    filter = {user_id: @user.id}
    page = 0
    if params.has_key?(:site)
      site_req = params.require(:site).permit(:id,:page)
      if(site_req.has_key?(:id))
        filter[:site_id] = params.require(:site).permit(:id)[:id]
      end
      if(site_req.has_key?(:page))
        page = (params.require(:site).permit(:page)[:page].to_i-1)*10
      end
    end
    sites = Subscriber.joins("left join sites on sites.id = subscribers.site_id").select("sites.id").where(filter)
    site = []
    site.clear
    sites.each{|i| site << i["id"]}
    render json: Article.joins("left join sites on sites.id = articles.site_id").select("articles.id,sites.host,articles.title,articles.link,articles.description,articles.published_at").where(site_id: site).order(published_at: :desc).limit(10).offset(page)
  end
  private

  def site_params
    params.require(:site).permit(:link) if params.required(:site)
  end
end
