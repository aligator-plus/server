class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  before_action :authorize
  def authorize
  	authorization = request.headers['Authorization']
    unless authorization.nil?
      auth = authorization.split(" ")
      if auth[0] == "Token" && auth.length > 1
        auth_params = auth[1].split(".")
        @user = User.find_by(id: auth_params[0], remember_token: auth_params[1])
      end
    end
    if (!@user  &&  request.env['PATH_INFO'] != "/signup" &&  request.env['PATH_INFO'] != "/login" &&  request.env['PATH_INFO'] != "/" &&  !(request.env['PATH_INFO'].include? '/users'))
        render json: { :status => 401, :response => { :error => "Invalid token" } }
    end
  end
  def index
    puts "YEEAHHH"
  end
end
