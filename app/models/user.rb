class User < ApplicationRecord
  before_save { email.downcase! }

  validates :username, :presence => true, :length => { within: 4..32 },
      :uniqueness => { case_sensitive: false }

  validates :email, :presence => true, :length => { maximum: 255 },
      :format => { with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i },
      :uniqueness => { case_sensitive: false }

  has_secure_password
  validates :password, :presence => true, :length => { within: 8..40 },
      :confirmation => true

  validates :password_confirmation, :presence => true
  has_many :subscribers
  has_many :sites, through: :subscribers

  before_create :create_remember_token
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private
    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
end
