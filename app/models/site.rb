require 'nokogiri'
require 'net/http'
require 'rss'

class Site < ApplicationRecord
  before_validation :check_site_options
  validates :rss, :host, presence: true, uniqueness: true
  has_many :articles, dependent: :destroy
  has_many :subscribers
  has_many :users, through: :subscribers

  protected
    def check_site_options
      link = self.rss
      link = "https://#{link}" if link.match(/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix).nil?
      host_match = link.match(/^(?:http|https):\/\/([a-z0-9\-\.]+)/i)
      puts link

      if host_match.nil? || host_match.length < 2
        self.errors[:base] << "RSS link is not correct"
        return false
      end

      puts "Parsing host " + host_match[1] + ", checking link " + link + " ..."
      begin
        rss_feed = Hash.new
        response = get_response_with_redirect(link)
        puts "Host content-type: " + response['Content-Type']

        if (response['Content-Type'].include? 'application/rss+xml') || (response['Content-Type'].include? 'text/xml') || (response['Content-Type'].include? 'application/xml')
          puts "RSS feed found."
          rss_feed['link'] = link
        else
          puts "Site page found"
          page = Nokogiri::HTML(response.body)
          elements = page.css('link[type="application/rss+xml"], a[type="application/rss+xml"]')
          if elements.length > 0
            rss_feed['link'] = elements[0]['href']
            unless rss_feed['link'].match(/^(?:http|https):\/\//)
              rss_feed['link'] = host_match[0] + (rss_feed['link'][0] == '/' ? '' : '/') + rss_feed['link']
            end
            puts "rss: " + rss_feed['link']
            response = get_response_with_redirect(rss_feed['link'])
          end
        end
        unless rss_feed['link'].nil?
          rss = RSS::Parser.parse(response.body)
          rss_feed['title'] = rss.channel.title
          rss_feed['host'] = host_match[1]
          self.title = rss_feed['title']
          self.host = rss_feed['host']
          self.rss = rss_feed['link']
        end
      rescue Exception => e
        self.errors[:base] << e.message
        return false
      end
    end

    def get_response_with_redirect(link)
      link = URI(link)
      r = Net::HTTP.get_response(link)
      if r.code == "301"
        r = get_response_with_redirect(r['Location'])
      end
      r
    end
end
