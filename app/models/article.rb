class Article < ApplicationRecord
  belongs_to :site
  validates :link, presence: true, uniqueness: true
end
