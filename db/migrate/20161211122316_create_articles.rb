class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.belongs_to :site, index: true, foreign_key: true
      t.string :title
      t.string :link
      t.text :description
      t.datetime :published_at

      t.timestamps
    end
    add_index :articles, [:site_id, :link], unique: true
  end
end
