class CreateSubscribers < ActiveRecord::Migration[5.0]
  def change
    create_table :subscribers do |t|
      t.belongs_to :site
      t.integer :user_id
      t.integer :site_id
      t.timestamps
    end
  end
end
