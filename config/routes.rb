Rails.application.routes.draw do
  get 'sessions/new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  resources :users

  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'

  post '/subscribe', to: 'sites#create'
    post '/getusername', to: 'sites#getusername'
  post '/getsites', to: 'sites#getsite'
  post '/getarticles', to: 'sites#getarticle'

  post '/detect', to: 'articles#detectAll'
end
