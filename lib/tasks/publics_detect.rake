require 'net/http'
require 'rss'

namespace :publics do
  desc "Update the database with new articles"
  task :detect => :environment do
    puts "Run an articles detector"
    sites = Site.all
    sites.each do |site|
      response = Net::HTTP.get_response(URI(site.rss))
      # puts "SITE RSS: "
      # puts site.rss
      rss = RSS::Parser.parse(response.body)
      unless rss.nil?
        rss.items.each do |item|
          Article.create(:site => site,
            :title => item.title,
            :link => item.link,
            :description => item.description,
            :published_at => item.pubDate)
        end
      end
    end
  end
end
