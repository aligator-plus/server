require 'rails_helper'

describe User do
  it { is_expected.to have_db_column(:username).of_type(:string)}
  it { is_expected.to have_db_column(:email).of_type(:string)}
  it { is_expected.to have_db_column(:password_digest).of_type(:string)}
end