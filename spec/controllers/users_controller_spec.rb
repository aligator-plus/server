require 'rails_helper'

describe UsersController do
  describe 'GET :index' do
      let!(:user) { create(:user, 
        username: 'Kisya', 
        email: 'bla@ukr.net', 
        password: 'password', 
        password_confirmation: 'password') }
      it 'returns users' do
        process :index, method: :get
        expect(assigns(:users)).to eq nil
      end
  end
  describe 'POST :create' do
      it 'creates user' do
        process :create, method: :post, params: {
          user: {
            username: 'kisya',
            email: 'bla@ukr.net',
            password: 'password', 
            password_confirmation: 'password'
          }
        }
        expect(User.count).to eq 1
        user = User.first
        expect(user.username).to eq 'kisya'
        expect(user.email).to eq 'bla@ukr.net'
      end
  end
  describe 'GET :show' do
    it 'shows user' do

    end 
  end
end
