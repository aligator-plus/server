FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "post username #{n}" }
  end
end